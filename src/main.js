import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import VueJquery from 'vue-jquery';

Vue.use(VueRouter);
Vue.use(VueJquery);

import Connexion from './views/Connexion.vue'
import Home from './views/Home.vue'
import POR from './views/POR.vue'
import PTV from './views/PTV.vue'
import MA from './views/MA.vue'

const routes = [
    {path: '/', name:"Connexion", component: Connexion},
    {path: '/Home',name:"Home", component: Home},
    {path: '/POR',name:"POR", component: POR},
    {path: '/PTV',name:"PTV", component: PTV},
    {path: '/MA',name:"MA", component: MA}
];

const router = new VueRouter({
  routes
});

Vue.config.productionTip = false;

new Vue({
    router,
  render: h => h(App),
}).$mount('#app');
